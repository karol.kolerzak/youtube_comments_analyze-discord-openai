from pyairtable import Api
import discord
import asyncio
import matplotlib.pyplot as plt
from discord.ext import commands
from credentials import *


class YouTubeBot(commands.Cog):
    def __init__(self, bot, airtable):
        self.status = True
        self.bot = bot
        self.table_queue = airtable.table(AIRTABLE_BASE, AIRTABLE_TABLE_QUEUE)
        self.table_published = airtable.table(AIRTABLE_BASE, AIRTABLE_TABLE_PUBLISHED)
        self.table_non_published = airtable.table(AIRTABLE_BASE, AIRTABLE_TABLE_NON_PUBLISHED)

    @commands.Cog.listener()
    async def on_ready(self):
        print('Bot ready')
        self.bot.loop.create_task(self.analyze_comments())

    @commands.command()
    async def analyze(self, ctx, video_id=None, *args):
        self.table_queue.create({"youtube_id": video_id, "message_id": str(ctx.message.id)})
        await ctx.message.reply("Added to queue")

    async def send_reply(self, message_id, reply=None, file=None, embed=None):
        await self.bot.wait_until_ready()
        channel = self.bot.get_channel(DISCORD_CHANNEL)

        if channel:
            if not self.bot.is_closed():
                message = await channel.fetch_message(message_id)
                await message.reply(content=reply, file=file, embed=embed)
        else:
            print("Channel not found")

    async def remove_plot(self, video_id):
        if os.path.exists(f'{video_id}.png'):
            os.remove(f'{video_id}.png')
            return

    def get_data(self):
        record = self.table_non_published.first()
        if record:
            record_id = record.get('id')
            record_fields = record.get('fields')
            correct = record_fields.get('correct')

            if correct == None:
                message_id = record_fields.get('message_id')
                self.send_reply(message_id, "Not correct data")
                self.table_non_published.delete(id)
                return None

            data = {
                "record_id": record_id,
                "message_id": record_fields.get('message_id'),
                "video_id": record_fields.get('youtube_id'),
                "positive_comments_amount": record_fields.get('positive'),
                "negative_comments_amount": record_fields.get('negative'),
                "neutral_comments_amount": record_fields.get('neutral'),
                "other_comments_amount": record_fields.get('other'),
                "positive_example": record_fields.get('positive_example'),
                "negative_example": record_fields.get('negative_example'),
                "neutral_example": record_fields.get('neutral_example')
            }
            return data
        return None

    def add_to_table_published(self, video_id, message_id, positive_comments_amount=0, negative_comments_amount=0,
                               other_comments_amount=0, positive_example=None, negative_example=None,
                               neutral_example=None):
        self.table_published.create(
            {"youtube_id": video_id, "message_id": message_id,
             "positive": positive_comments_amount, "negative": negative_comments_amount,
             "neutral": negative_comments_amount, "other": other_comments_amount,
             "positive_example": positive_example, "negative_example": negative_example,
             "neutral_example": neutral_example})
        return

    async def make_plot(self, video_id, positive_comments, negative_comments, neutral_comments, other_comments):
        kategorie = ['Positive', 'Negative', 'Neutral', 'Other']
        wartosci = [positive_comments, negative_comments, neutral_comments, other_comments]
        plt.bar(kategorie, wartosci, color=['green', 'red', 'gray', 'black'])

        plt.title('Comments Analyze')
        plt.xlabel('Category')
        plt.ylabel('Amount')
        plt.savefig(f'{video_id}.png')

    async def make_embed(self, video_id, positive_comments, negative_comments, neutral_comments, positive_example,
                         negative_example,
                         neutral_example):
        embed = discord.Embed(
            title='Comments Analyze',
            color=discord.Color.yellow()
        )

        embed.set_author(name=f"Video id: {video_id}")
        embed.add_field(name='Positive', value=positive_comments, inline=True)
        embed.add_field(name='Negative', value=negative_comments, inline=True)
        embed.add_field(name='Neutral', value=neutral_comments, inline=True)

        embed.add_field(name='Positive example', value=positive_example, inline=False)
        embed.add_field(name='Negative example', value=negative_example, inline=False)
        embed.add_field(name='Neutral example', value=neutral_example, inline=False)

        embed.set_image(url=f"attachment://{video_id}.png")
        return embed

    async def analyze_comments(self):
        while self.status:
            data = self.get_data()
            if data is not None:
                record_id = data.get("record_id", None)
                video_id = data.get("video_id", None)
                message_id = data.get("message_id", None)
                positive_comments_amount = data.get("positive_comments_amount", None)
                negative_comments_amount = data.get("negative_comments_amount", None)
                neutral_comments_amount = data.get("neutral_comments_amount", None)
                other_comments_amount = data.get("other_comments_amount", None)
                positive_example = data.get("positive_example", None)
                negative_example = data.get("negative_example", None)
                neutral_example = data.get("neutral_example", None)

                await self.make_plot(video_id, positive_comments_amount, negative_comments_amount,
                                     neutral_comments_amount,
                                     other_comments_amount)

                embed = await self.make_embed(video_id, positive_comments_amount, negative_comments_amount,
                                              neutral_comments_amount, positive_example, negative_example,
                                              neutral_example)

                file = discord.File(f"{video_id}.png", filename=f"{video_id}.png")

                self.add_to_table_published(video_id, message_id, positive_comments_amount, negative_comments_amount,
                                            other_comments_amount, positive_example, negative_example,
                                            neutral_example)

                self.table_non_published.delete(record_id)

                await self.send_reply(message_id, file=file, embed=embed)
                await self.remove_plot(video_id)

            else:
                print('No records')
                await asyncio.sleep(10)


async def main():
    intents = discord.Intents.default()
    intents.message_content = True
    bot = commands.Bot(command_prefix='$', intents=intents)

    airtable = Api(AIRTABLE_KEY)

    await bot.add_cog(YouTubeBot(bot, airtable))
    await bot.start(DISCORD_KEY)


if __name__ == "__main__":
    asyncio.run(main())
