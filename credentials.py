from dotenv import load_dotenv
import os

load_dotenv()
DISCORD_KEY = os.getenv("DISOCRD_KEY")
OPENAI_KEY = os.getenv("OPENAI_KEY")
YOUTUBE_KEY = os.getenv("YOUTUBE_KEY")

AIRTABLE_KEY = os.getenv("AIRTABLE_KEY")
AIRTABLE_BASE = os.getenv("AIRTABLE_BASE")
AIRTABLE_TABLE_NON_PUBLISHED = os.getenv("AIRTABLE_TABLE_NON_PUBLISHED")
AIRTABLE_TABLE_PUBLISHED = os.getenv("AIRTABLE_TABLE_PUBLISHED")
AIRTABLE_TABLE_QUEUE = os.getenv("AIRTABLE_TABLE_QUEUE")

discord_channel_str = os.getenv("DISCORD_CHANNEL")
DISCORD_CHANNEL = int(discord_channel_str) if os.getenv("DISCORD_CHANNEL") else None

THREADS_NUMBER = 100