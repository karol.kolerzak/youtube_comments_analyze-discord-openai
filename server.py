import threading
import time

from pyairtable import Api
from queue import Queue
from googleapiclient.discovery import build, HttpError
from openai import OpenAI
from credentials import *



class Server():
    def __init__(self, airtable, openai):
        self.client = openai
        self.status = True
        self.threads = []
        self.semaphore = threading.Semaphore(THREADS_NUMBER)
        self.table_non_published = airtable.table(AIRTABLE_BASE, AIRTABLE_TABLE_NON_PUBLISHED)
        self.table_queue = airtable.table(AIRTABLE_BASE, AIRTABLE_TABLE_QUEUE)
        self.result_queue = Queue()
        self.comments_queue = None
        self.lock = threading.Lock()

    def start_work(self):
        while self.status:
            record = self.table_queue.first()
            if record:
                record_id = record.get('id')
                record_fields = record.get('fields')
                video_id = record_fields.get('youtube_id')
                message_id = record_fields.get('message_id')

                self.comments_queue = self.download_comments(video_id, id)

                if self.comments_queue is None:
                    self.handle_youtube_error(video_id, record_id, message_id)
                    continue

                while self.comments_queue.qsize() != 0:
                    comment = self.get_comment()
                    if self.semaphore.acquire(blocking=False):
                        thread = threading.Thread(target=self.comment_analyze_by_chat, args=(comment,))
                        thread.start()
                        self.threads.append(thread)
                    else:
                        continue

                for thread in self.threads:
                    thread.join()

                data_to_database = self.sort_comments()

                self.add_to_database(video_id, message_id, data_to_database)
                self.table_non_published.delete(record_id)

                self.reset_variable()
                print('Task finished')
            else:
                print('No rocords')
                time.sleep(5)

    def stop_work(self):
        self.status = False

    def get_comment(self):
        return self.comments_queue.get()

    def put_comment(self, status, comment):
        self.lock.acquire()
        self.result_queue.put((status, comment))
        self.lock.release()

    def download_comments(self, video_id, record_id):
        comments_queue = Queue()
        with build('youtube', 'v3', developerKey=YOUTUBE_KEY) as youtube:
            try:
                video_response = youtube.commentThreads().list(
                    part='snippet,replies',
                    videoId=video_id,
                ).execute()
            except HttpError:
                return None

            while video_response:
                for item in video_response['items']:
                    comment = item['snippet']['topLevelComment']['snippet']['textDisplay']
                    comments_queue.put(comment)

                if 'nextPageToken' in video_response:
                    video_response = youtube.commentThreads().list(
                        part='snippet',
                        videoId=video_id,
                        pageToken=video_response['nextPageToken']
                    ).execute()
                else:
                    break

        return comments_queue

    def handle_youtube_error(self, video_id, record_id, message_id):
        self.table_non_published.create(
            {"youtube_id": video_id, "message_id": message_id, "correct": False})
        self.table_non_published.delete(record_id)
        print('Not correct data')

    def comment_analyze_by_chat(self, comment):
        completion = self.client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "system",
                 "content": "Evaluate whether a comment is positive, neutral or negative. Return only 'Positive', 'Neutral' or 'Negative'. If comment is not about opinion return 'other'"},
                {"role": "user", "content": comment}
            ]
        )
        status = completion.choices[0].message.content
        self.put_comment(status, comment)

    def reset_variable(self):
        self.result_queue = Queue()

    def sort_comments(self):
        positive_comments = []
        negative_comments = []
        neutral_comments = []
        other_comments = []

        comments_num = self.result_queue.qsize()

        for _ in range(comments_num):
            status, comment = self.result_queue.get()
            if status == "Positive":
                positive_comments.append(comment)
            elif status == "Negative":
                negative_comments.append(comment)
            elif status == "Neutral":
                neutral_comments.append(comment)
            else:
                other_comments.append(comment)

        positive_example = positive_comments[0] if len(positive_comments) > 0 else ''
        negative_example = negative_comments[0] if len(negative_comments) > 0 else ''
        neutral_example = neutral_comments[0] if len(neutral_comments) > 0 else ''

        result = {"comments_number": comments_num,
                  "positive": len(positive_comments),
                  "negative": len(negative_comments),
                  "neutral": len(neutral_comments),
                  "other": len(other_comments),
                  "positive_example": positive_example,
                  "negative_example": negative_example,
                  "neutral_example": neutral_example
                  }

        return result

    def add_to_database(self, video_id, message_id, comments):
        data = comments
        data["youtube_id"] = video_id
        data["correct"] = True
        data["message_id"] = message_id
        self.table_non_published.create(data)


def main():
    airtable = Api(AIRTABLE_KEY)
    openai = OpenAI(api_key=OPENAI_KEY)
    server = Server(airtable, openai)

    server_thread = threading.Thread(target=server.start_work)
    server_thread.start()

    input("Press Enter to stop server\n")
    server.stop_work()
    server_thread.join()


if __name__ == "__main__":
    main()
