# YouTube Comments Analyzer 
### YouTube + Discord Bot + OpenAI + Airtable Database

![](discord.gif)


## Next Step
* Add docker and webhooks to project

## Setup

1. **Clone the repository:**
    ```bash
    git clone https://gitlab.com/karol.kolerzak/youtube_comments_analyze-discord-openai 
   ```

2. **Install dependencies:**
    ```bash
    pip install -r requirements.txt
    ```

3. **Set up environment variables:**

    - Create a `.env` file in the root directory.
    - Add the following environment variables to the `.env` file:
        ```dotenv
        DISCORD_KEY=your_discord_bot_token
        DISCORD_CHANNEL=your_discord_channel
        OPENAI_KEY=your_openai_api_key
        YOUTUBE_KEY=your_youtube_api_key
        AIRTABLE_KEY=your_airtable_api_key
        AIRTABLE_BASE=your_airtable_base_id
        AIRTABLE_TABLE=your_airtable_table_name
        AIRTABLE_TABLE_NON_PUBLISHED=your_airtable_table_nonpublished
        AIRTABLE_TABLE_PUBLISHED=your_airtable_table_published
        AIRTABLE_TABLE_QUEUE=your_airtable_table_queue
        ```

   
4. **Run the bot:**
    ```bash
    python server.py
    python bot.py
    ```

